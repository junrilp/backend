<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuestionnaires extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if( ! Schema::hasTable( 'questionnaires' ) ) {

            Schema::create('questionnaires', function (Blueprint $table) {

                $table
                ->bigIncrements('id');

                $table
                ->text('questions_id')
                ->nullable();

                $table
                ->text('title')
                ->nullable();

                $table
                ->text('image')
                ->nullable();

                $table
                ->integer('user_id')
                ->nullable();

                $table
                ->timestamps();

            });

        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('questionnaires');
    }
}
