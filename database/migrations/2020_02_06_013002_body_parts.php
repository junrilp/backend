<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class BodyParts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      if( ! Schema::hasTable( 'body_parts' ) ) {

          Schema::create('body_parts', function (Blueprint $table) {

              $table->bigIncrements('id');

              $table->text('name');

              $table->timestamps();

          });
          
      }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('body_parts');
    }
}
