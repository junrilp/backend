<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuestions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if( ! Schema::hasTable( 'questions' ) ) {

            Schema::create('questions', function (Blueprint $table) {

                $table
                ->bigIncrements('id');

                $table
                ->text('question')
                ->nullable();

                $table
                ->enum('level', array('primary','secondary'))
                ->nullable();

                $table
                ->enum('type', array('decimal','integer','choices','any'))
                ->nullable();

                $table
                ->boolean('status') 
                ->nullable();

                $table
                ->text('image')
                ->nullable();

                $table
                ->text('body_parts')
                ->nullable();
                
                $table
                ->text('problems')
                ->nullable();
                
                $table
                ->boolean('isProceed') 
                ->nullable();

                $table
                ->integer('question_link_id')
                ->nullable();

                $table
                ->timestamps();

            } );

        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('questions');
    }
}
