<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuestionChoices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if( ! Schema::hasTable( 'question_choices' ) ) {

            Schema::create('question_choices', function (Blueprint $table) {

                $table
                ->bigIncrements('id');

                $table
                ->text('choice')
                ->nullable();

                $table
                ->integer('question_id')
                ->nullable();

                $table
                ->integer('question_link_id')
                ->nullable();

                $table->timestamps();

            });

        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('question_choices');
    }
}
