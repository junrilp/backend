<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Support\Facades\Schema;

use App\Models\Questions as Questions;

class QuestionChoices extends Model
{
  
  protected $fillable = [
            
    'choice', 

    'question_id',

    'question_link_id'

  ];

  public function questions()
  {
    return $this->hasMany(Questions::class, 'question_id');
  }

  public function tableColumn() {

    return Schema::getColumnListing('question_choices');

  }
  
}
