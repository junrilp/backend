<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Support\Facades\Schema;

class Questionnaires extends Model
{
  protected $fillable = [

    'questions_id',

    'title',

    'user_id',

    'image'

  ];


  public function tableColumn() {

    return Schema::getColumnListing('questionnaires');

  }

}
