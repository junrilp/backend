<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Support\Facades\Schema;

class Problems extends Model
{
  protected $fillable = [
            
    'name'

  ];


  public function tableColumn() {

    return Schema::getColumnListing('problems');

  }
  
  
}
