<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Support\Facades\Schema;

class BodyParts extends Model
{
  
    public function list_body_parts() {

        $body_parts = [

            [
                'id' => 1,
                'name' => "Head"
            ],
            [
                'id' => 2,
                'name' => "Arm"
            ],
            [
                'id' => 3,
                'name' => "Back"
            ],
            [
                'id' => 4,
                'name' => "Waist"
            ],
            [
                'id' => 5,
                'name' => "Buttocks/Backside "
            ],
            [
                'id' => 6,
                'name' => "Leg"
            ],
            [
                'id' => 7,
                'name' => "Face"
            ],
            [
                'id' => 8,
                'name' => "Chest"
            ],
            [
                'id' => 9,
                'name' => "Stomach"
            ],
            [
                'id' => 10,
                'name' => "Hip"
            ]
           
        ];

        return $body_parts;

    }
  
}
