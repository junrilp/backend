<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Support\Facades\Schema;

class SurgicalProcedure extends Model
{
  protected $fillable = [
            
    'procedure_name'

  ];


  public function tableColumn() {

    return Schema::getColumnListing('surgical_procedures');

  }
  
  
}
