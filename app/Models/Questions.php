<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Support\Facades\Schema;

class Questions extends Model
{
  protected $fillable = [
            
    'question', 

    'level', 

    'type', 
    
    'status', 

    'image',  

    'body_parts', 

    'problems', 

    'isProceed', 

    'question_link_id'

  ];

  public function question_choices()
  {
    return $this->hasMany(QuestionChoices::class, 'question_id');
  }

  public function tableColumn() {

    return Schema::getColumnListing('questions');

  }
  
}
