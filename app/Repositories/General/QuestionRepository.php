<?php

namespace App\Repositories\General;

use App\Models\Questions;

use App\Repositories\BaseRepository;

use Illuminate\Support\Facades\DB;

class QuestionRepository extends BaseRepository
{


  public function model() {

    return Questions::class;

  }


  public function findById( $id ) {

    return $this -> model -> find( $id );

  }


  public function update( Questions $question, array $data ): Questions {

    return DB::transaction( function () use ( $question, $data ) {

      $items = [];

      foreach ( $data as $key => $value ) {

        $items[ $key ] = $value;

      }

      if ( $question -> update( $items ) ) {

        return $question;

      }

    } );

  }


  public function create( array $data ) : Questions {

    return DB::transaction( function () use ( $data ) {

      if ( $question = parent::create( [

        'question' => $data[ 'question' ],

        'level' => $data[ 'level' ],

        'type' => $data[ 'type' ],

        'status' => $data[ 'status' ],

        'image' => $data[ 'image' ],

        'body_parts' => $data[ 'body_parts' ],

        'problems' =>$data[ 'problems' ],

        'isProceed' => $data[ 'isProceed' ]

      ] ) ) {

        return $question;

      }

    } );

  }


  public function getTableColumn() {

    return $this -> model -> tableColumn();

  }


}
