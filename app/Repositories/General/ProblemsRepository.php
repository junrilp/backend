<?php

namespace App\Repositories\General;

use App\Models\Problems;

use App\Repositories\BaseRepository;

use Illuminate\Support\Facades\DB;

class ProblemsRepository extends BaseRepository
{
 

  public function model() {

    return Problems::class;

  }


  public function findById( $id ) {                        

    return $this -> model -> find( $id );

  }


  public function update( Problems $problem, array $data ): Problems {

    return DB::transaction( function () use ( $problem, $data ) {

      $items = [];

      foreach ( $data as $key => $value ) {

        $items[ $key ] = $value;

      }

      if ( $problem -> update( $items ) ) {

        return $problem;

      }

    } );

  }


  public function create( array $data ) : Problems {

    return DB::transaction( function () use ( $data ) {
      
      if ( $problem = parent::create( [

        'name' => $data[ 'name' ],

      ] ) ) {
        
        return $problem;

      }

    } );

  }
 

  public function getTableColumn() {

    return $this -> model -> tableColumn();

  }


}
 