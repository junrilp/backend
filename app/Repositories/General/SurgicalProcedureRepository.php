<?php

namespace App\Repositories\General;

use App\Models\SurgicalProcedure;

use App\Repositories\BaseRepository;

use Illuminate\Support\Facades\DB;

class SurgicalProcedureRepository extends BaseRepository
{
 

  public function model() {

    return SurgicalProcedure::class;

  }


  public function findById( $id ) {                        

    return $this -> model -> find( $id );

  }


  public function update( SurgicalProcedure $surgical, array $data ): SurgicalProcedure {

    return DB::transaction( function () use ( $surgical, $data ) {

      $items = [];

      foreach ( $data as $key => $value ) {

        $items[ $key ] = $value;

      }

      if ( $surgical -> update( $items ) ) {

        return $surgical;

      }

    } );

  }


  public function create( array $data ) : SurgicalProcedure {

    return DB::transaction( function () use ( $data ) {
      
      if ( $surgical = parent::create( [

        'procedure_name' => $data[ 'procedure_name' ],

      ] ) ) {
        
        return $surgical;

      }

    } );

  }
 

  public function getTableColumn() {

    return $this -> model -> tableColumn();

  }


}
 