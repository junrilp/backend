<?php

namespace App\Repositories\General;

use App\Models\Questionnaires;

use App\Repositories\BaseRepository;

use Illuminate\Support\Facades\DB;

class QuestionnairesRepository extends BaseRepository
{


  public function model() {

    return Questionnaires::class;

  }


  public function findById( $id ) {

    return $this -> model -> find( $id );

  }


  public function update( Questionnaires $questionnaires, array $data ): Questionnaires {

    return DB::transaction( function () use ( $questionnaires, $data ) {

      $items = [];

      foreach ( $data as $key => $value ) {

        $items[ $key ] = $value;

      }

      if ( $questionnaires -> update( $items ) ) {

        return $questionnaires;

      }

    } );

  }


  public function create( array $data ) : Questionnaires {

    return DB::transaction( function () use ( $data ) {

      if ( $questionnaires = parent::create( [

        'questions_id' => $data[ 'questions_id' ],

        'title' => $data[ 'title' ],

        'user_id' => $data[ 'user_id' ],

        'image' => $data[ 'image' ]

      ] ) ) {

        return $questionnaires;

      }

    } );

  }


  public function getTableColumn() {

    return $this -> model -> tableColumn();

  }


}
