<?php

namespace App\Repositories\General;

use App\Models\QuestionChoices;

use App\Repositories\BaseRepository;

use Illuminate\Support\Facades\DB;

class QuestionChoicesRepository extends BaseRepository
{
 

  public function model() {

    return QuestionChoices::class;

  }


  public function findById( $id ) {                        

    return $this -> model -> find( $id );

  }


  public function update( QuestionChoices $question_choices, array $data ): QuestionChoices {

    return DB::transaction( function () use ( $question_choices, $data ) {

      $items = [];

      foreach ( $data as $key => $value ) {

        $items[ $key ] = $value;

      }

      if ( $question_choices -> update( $items ) ) {

        return $question_choices;

      }

    } );

  }   


  public function create( array $data ) : QuestionChoices {

    return DB::transaction( function () use ( $data ) {
      
      if ( $question_choices = parent::create( [

        'choice' => $data[ 'choice' ],

        'question_id' => $data[ 'question_id' ],

        'question_link_id' => ( ! empty( $data['question_link_id' ] ) ? $data['question_link_id' ] : NULL )

      ] ) ) {
        
        return $question_choices;

      }

    } );

  }
 

  public function getTableColumn() {

    return $this -> model -> tableColumn();

  }


}
 