<?php


// get all route files
if (!function_exists('include_route_files')) {
    /**
     * Loops through a folder and requires all PHP files
     * Searches sub-directories as well.
     *
     * @param $folder
     */
    function include_route_files($folder)
    {
        try {
            $rdi = new recursiveDirectoryIterator($folder);
            $it = new recursiveIteratorIterator($rdi);

            while ($it->valid()) {
                if (!$it->isDot() && $it->isFile() && $it->isReadable() && $it->current()->getExtension() === 'php') {
                    require $it->key();
                }

                $it->next();
            }
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }
}


/* =====> For multi-language helper function  <===== */

// from form data to json string
if (!function_exists('string_to_json')) {
    function string_to_json($lang, $fields = [], $formData = [])
    {
        $data = [];
        foreach ($formData as $key => $value) {
            $json = $value;
            if (in_array($key, $fields)) {
                $json = string_add_json($lang, $value, []);
            }
            $data[$key] = $json;
        }
        return $data;
    }
}

// get current language
if (!function_exists('lang')) {
    function lang()
    {
        return 'en';
        // return (!is_null(session()->get('locale'))) ? session()->get('locale') : 'en';
    }
}


if (!function_exists('string_to_value')) {
    function string_to_value($key, $string)
    {
        $array = string_to_array($string);

        if (!empty($array)) {
            foreach ($array as $arKey => $value) {
                if ($arKey === $key) {
                    return $value;
                }
            }
        }
        return null;
    }
}

// return json data to array
if (!function_exists('object_to_array')) {
    function object_to_array($lang = '', $fields, $items = array())
    {
        $data = [];
        if (!empty($items)) {
            foreach ($items as $ik => $item) {
                $key = false;
                $object = [];
                foreach ($fields as $field) {
                    $array = string_to_array($item->$field);
                    if (is_array($array)) {
                        foreach ($array as $ak => $arr) {
                            if ($lang === $ak) {
                                $object[$field] = $arr;
                                $key = true;
                            }
                            if ($lang === '' or $lang === null) {
                                $object[$field] = $arr;
                                $key = true;
                            }
                        }
                    } else {
                        $object[$field] = $item->$field;
                    }
                }
                if ($key) {
                    $data[] = $object;
                }
            }
        }
        return $data;
    }
}


// convert string to array
if (!function_exists('string_to_array')) {
    function string_to_array($string)
    {
        $arr = json_decode($string, true);
        return $arr;
    }
}


// array merge to json format
if (!function_exists('string_add_json')) {
    function string_add_json($key, $value, $arrList = [])
    {
        $array = [];
        if (!empty($arrList)) {
            $array = array_merge($arrList, [$key => $value]);
        } else {
            $array = [
                $key => $value
            ];
        }
        return json_encode($array);
    }
}

// Remove string from array
if (!function_exists('string_remove')) {
    function string_remove($lang, $string)
    {
        $arrays = string_to_array($string);

        if (is_array($arrays) AND array_key_exists($lang, $arrays))
          unset($arrays[$lang]);

        return $arrays;
    }
}

// Check inf the data is exist
if (!function_exists('is_data_exist')) {
    function is_data_exist($field, $name, $arrayList = [])
    {
        foreach ($arrayList as $data) {
            if (strtolower($data[$field]) === strtolower(trim($name))) {
                return true;
            }
        }
        return false;
    }
}
