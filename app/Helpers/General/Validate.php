<?php


namespace App\Helpers\General;

use Illuminate\Support\Facades\DB;

// validate class for register input

class Validate
{
  private
    $_passed = false,
    $_errors = array(),
    $_query = null,
    $_lang = null;

  function __construct($lang = null)
  {
    $this->_lang = $lang;
    // $this->_query = new query;
  }

  /**
   * @param $source
   * @param array $items
   * @return $this
   */
  public function check($source, $items = array())
  {
    $value = '';

    foreach ($items as $item => $rules) {
      foreach ($rules as $rule => $rule_value) {
        $value = trim($source[$item]);
        $item  = htmlentities($item, ENT_QUOTES, 'UTF-8');

        if ($rule === 'required' && empty($value)) {
          $this->addError($item, [ucwords($item) . " is required."]);
        } else if (!empty($value)) {

          switch ($rule) {
            case 'min':
              if (strlen($value) < $rule_value) {
                $this->addError($item, [ucwords($item) . " must be minimum of {$rule_value} character"]);
              }
              break;
            case 'max':
              if (strlen($value) > $rule_value) {
                $this->addError($item, ["{$item} must be Maximum of {$rule_value} character"]);
              }
              break;
            case 'matches':
              if ($value != $source[$rule_value]) {
                $this->addError($item, ["{$rule_value} must be matches of {$item}"]);
              }
              break;
            case 'numeric':
              if (!ctype_digit($item_value) && $rule_value) {
                $this->addError($item, [ucwords($item) . ' should be numeric']);
              }
              break;
            case 'alpha':
              if (!ctype_alpha($item_value) && $rule_value) {
                $this->addError($item, [ucwords($item) . ' should be alphabetic characters']);
              }
              break;
            case 'unique':
              $db = DB::table($rule_value);
              if (!is_null($this->_lang)) {
                $data = $db->get();
                foreach ($data as $val) {
                  $jsonVal = string_to_value($this->_lang, $val->$item);
                  if (strtolower($jsonVal) === strtolower($value)) {
                    $this->addError($item, [ucwords($value) .  " must be exist."]);
                  }
                }
              } else {
                $data = $db->where($item, strtolower($value));
                if ($data->count() > 0) {
                  $this->addError($item, [ucwords($value) . " must be exist."]);
                }
              }
              break;
          }
        }
      }
    }
    if (empty($this->_errors)) {
      $this->_passed = true;
    }
    return $this;
  }

  private function addError($name, $error)
  {
    $this->_errors[$name] = $error;
  }

  public function errors()
  {
    return $this->_errors;
  }

  public function passed()
  {
    return $this->_passed;
  }
}
