<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Factory as Auth;
use Illuminate\Http\Request;
use Ixudra\Curl\CurlService;


class Authenticate
{
    /**
     * The authentication guard factory instance.
     *
     * @var Auth
     */
    protected $auth;

    /**
     * Create a new middleware instance.
     *
     * @param Auth $auth
     * @return void
     */
    public function __construct(Auth $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  Request  $request
     * @param Closure $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {

        $token = $request->query('api_token');

        $data = ['api_token' => $token];

        $cUrl_service = new CurlService();

        $response = $cUrl_service->to(config('access.api.check_auth'))
            ->withData($data)
            ->returnResponseObject()
            ->asJson(true)
            ->post();

        if ($response->content["status"] !== "success")  return response('Unauthorized.', 401);

        $request->query->add(['user_id' => $response->content["user_id"]]);

        return $next($request);
    }
}
