<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Ixudra\Curl\CurlService;


class UserRoleAdministrator
{

    private $middleware_user_role = 'administrator';

    /**
     * Handle an incoming request.
     *
     * @param  Request  $request
     * @param Closure $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        $user_id = $request->query('user_id');

        $data = [
            'user_id' => $user_id,
            'user_role' => $this->middleware_user_role
        ];


        $cUrl_service = new CurlService();

        $response = $cUrl_service->to(config('access.api.check_user_role'))
            ->withData($data)
            ->returnResponseObject()
            ->asJson(true)
            ->post();

        if ($response->content["status"] !== "success") return response('Unauthorized.', 401);

        return $next($request);
    }
}
