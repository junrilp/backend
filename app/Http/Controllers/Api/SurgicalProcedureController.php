<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

use App\Repositories\General\SurgicalProcedureRepository;

use Illuminate\Http\Request;

use DB;

class SurgicalProcedureController extends Controller
{

  protected $surgicalProcedureRepo;

  public function __construct( SurgicalProcedureRepository $surgicalProcedureRepo ) {

    $this -> surgicalProcedureRepo = $surgicalProcedureRepo;

  }


  public function index( Request $request ) {

    $lang = $request -> input( 'lang' );

    $show_data = $this -> show_data( $lang );

    return response() -> json( $show_data );

  }


  public function store( Request $request ) {

    $input = json_decode( $request -> input( 'data' ) );

    $lang = $request -> input( 'lang' );

      $surgical_check = $this -> surgicalProcedureRepo;

      if ( $surgical_check -> count() > 0 ) {

        foreach ( $surgical_check -> get() as $surgical_checks ) {

          $surgical_checks_ = $this -> view_surgical_procedures( $surgical_checks, $lang );

          if(  ucwords( $surgical_checks_[ 'procedure_name' ] ) == ucwords( $input -> procedure_name ) ) {

            $message = ucwords( string_to_value( $lang, $input -> procedure_name ) );

            return response() -> json( alert_duplicate( $message, $input ) );

          }

        }

      }

      $formData = array(

        'procedure_name' => ucwords( $input -> procedure_name )

      );

      $jsonData = string_to_json( $lang, [ 'procedure_name' ], $formData );

      $surgical = $this -> surgicalProcedureRepo -> create( $jsonData );

      if ( $surgical ) {

        $message = ucwords( string_to_value( $lang, $surgical[ 'procedure_name' ] ) );

        return response() -> json( alert_success( $message, $surgical ) );

      }

  }


  public function show( $id ) {

    $surgical = $this -> view_surgical_procedures( $this -> surgicalProcedureRepo -> findById( $id ) );

    return response() -> json( $surgical );

  }


  public function edit( $id ) {

    $surgical = $this -> view_surgical_procedures( $this -> surgicalProcedureRepo -> findById( $id ) );

    return response() -> json( $surgical );

  }


  public function update( Request $request ) {

    $formData = json_decode( $request -> input( 'data' ) );

    if( ! empty (  $formData -> id ) ) {

      $lang = $formData -> language;

      $surgical_check = $this -> surgicalProcedureRepo;

      if ( $surgical_check -> count() > 0 ) {

        foreach ( $surgical_check -> get() as $surgical_checks ) {

          $surgical_checks_ = $this -> view_surgical_procedures( $surgical_checks, $lang );

          if( ucwords( $surgical_checks_[ 'procedure_name' ] ) == ucwords( $formData -> procedure_name ) && $surgical_checks_[ 'id' ] != $formData -> id ) {

            $message = ucwords( string_to_value( $lang, $formData -> procedure_name ) );

            return response() -> json( alert_duplicate( $message, $formData ) );

          }

        }

      }

      $surgical = $this -> surgicalProcedureRepo -> findById( $formData -> id );

      $surgicalVal = string_add_json( $lang, ucwords( $formData -> procedure_name ), string_remove( $lang, $surgical -> procedure_name ) );

      $respQuestion = $this -> surgicalProcedureRepo -> update( $surgical, [

        'procedure_name' => $surgicalVal

      ] );

      if ( $respQuestion ) {

        $message = ucwords( string_to_value( $lang, $surgical -> procedure_name));

        return response() -> json( alert_update( $message, $respQuestion ) );

      }

    }

  }


  public function destroy( Request $request, $id ) {

    $data = $request -> all();

    $lang = $request -> input( 'lang' );

    $surgical = $this -> surgicalProcedureRepo -> findById( $id );

    if ( $this -> surgicalProcedureRepo -> deleteById( $id ) ) {

      $message = ucwords( string_to_value( $lang, $surgical -> procedure_name) );

      return response() -> json( alert_delete( $message ) );

    }

  }

  public function getProceduralName( Request $request, $id, $lang ) {

    $surgical = $this -> surgicalProcedureRepo -> findById( $id );

    $message = ucwords( string_to_value( $lang, $surgical -> procedure_name) );

    return response() -> json( [ 'procedure_name' => $message ] );


  }


  public function view_surgical_procedures( $data, $lang ) {

    return [

      'id' => $data -> id,

      'procedure_name' => string_to_value( $lang, $data -> procedure_name ),

      'created_at' => $data -> created_at,

      'updated_at' => $data -> updated_at,

    ];

  }

  public function show_data( $lang ) {

    $procedure = DB::table( 'surgical_procedures' )

          -> select( [ 'id', 'procedure_name', 'created_at', 'updated_at' ] )

          -> get();

    $json = $this -> get_data( $lang  , [ 'id', 'procedure_name', 'created_at', 'updated_at' ], $procedure );

    return $json;
  }

  public function get_data( $lang = '', $fields, $items = array() ) {

    $data = [];

    if ( ! empty( $items ) ) {

      foreach ( $items as $ik => $item ) {

        $key = false;

        $object = [];

        foreach ( $fields as $field ) {

          $array = $this -> to_array( $item -> $field );

          if ( is_array( $array ) ) {

            foreach ( $array as $ak => $arr ) {

              if ( $lang === $ak ) {

                $object[ $field ] = $arr;

                $key = true;

              }

              if ( $lang === '' or $lang === null ) {

                $object[ $field ] = $arr;

                $key = true;

              }

            }

          } else {

            $object[ $field ] = $item -> $field;

          }

        }

        if ( $key ) {

          $data[] = $object;

        }

      }

    }

    return (object) $data;

  }

  public function to_array( $string )  {

      $arr = json_decode( $string, true );

      return  $arr;

  }

}
