<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

use App\Repositories\General\ProblemsRepository;

use Illuminate\Http\Request;

use DB;

class ProblemsController extends Controller
{

  protected $problemsRepo;

  public function __construct( ProblemsRepository $problemsRepo) {

    $this -> problemsRepo = $problemsRepo;

  }


  public function index( Request $request ) {

    $lang = $request -> input( 'lang' );

    $show_data = $this -> show_data( $lang );

    return response() -> json( $show_data );

  }


  public function store( Request $request ) {

    $input = json_decode( $request -> input( 'data' ) );

    $lang = $request -> input( 'lang' );

    $problem_check = $this -> problemsRepo;

    if ( $problem_check -> count() > 0 ) {

      foreach ($problem_check -> get() as $problem_checks ) {

        $problem_checks_ = $this -> view_problems( $problem_checks );

        if(  ucwords( $problem_checks_[ 'name' ] ) == ucwords( $input -> name ) ) {

          $message = ucwords( string_to_value( $lang, $input -> name ) );

          return response() -> json( alert_duplicate( $message, $input ) );

        }

      }

    }

    $formData = array(

      'name' => ucwords( $input -> name )

    );

    $jsonData = string_to_json( $input -> language, [ 'name' ], $formData );

    $problem = $this -> problemsRepo -> create( $jsonData );

    if ( $problem ) {

      $message =  ucwords( string_to_value( $lang, $problem[ 'name' ] ) );

      return response() -> json( alert_success( $message, $problem ) );

    }

  }


  public function show( $id ) {

    $problems = $this -> view_problems( $this -> problemsRepo -> findById( $id ) );

    return response() -> json( $problems );

  }


  public function edit( $id ) {

    $question = $this -> view_problems( $this -> problemsRepo -> findById( $id ) );

    return response() -> json( $question );

  }


  public function update( Request $request ) {

    $formData = json_decode( $request -> input( 'data' ) );

    if( ! empty (  $formData -> id ) ) {

      $lang = $formData -> language;

      $problem_check = $this -> problemsRepo;

      if ( $problem_check -> count() > 0 ) {

        foreach ($problem_check -> get() as $problem_checks ) {

          $problem_checks_ = $this -> view_problems( $problem_checks );

          if( ucwords( $problem_checks_[ 'name' ] ) == ucwords( $formData -> name ) && $problem_checks_[ 'id' ] != $formData -> id ) {

            $message = ucwords( string_to_value( $lang, $formData -> name ) );

            return response() -> json( alert_duplicate( $message, $formData ) );

          }

        }

      }

      $problem = $this -> problemsRepo -> findById( $formData -> id );

      $problemVal = string_add_json( $lang, ucwords( $formData -> name ), string_remove( $lang, $problem -> name ) );

      $respProblem = $this -> problemsRepo -> update( $problem, [

        'name' => $problemVal

      ] );

      if ( $respProblem ) {

        $message = ucwords( string_to_value( $lang, $problem -> name));

        return response() -> json( alert_update( $message, $respProblem ) );

      }

    }

  }


  public function destroy( Request $request, $id ) {

    $data = $request -> all();

    $lang = $request -> input( 'lang' );

    $question = DB::table( "questions" ) -> get();

    $array = array();

    foreach( $question as $value ) {

      foreach( json_decode( $value -> problems ) as $probs ) {

         array_push( $array, $probs );

      }

    }

    if ( ! in_array( $id, $array ) ) {

      $problem = $this -> problemsRepo -> findById( $id );

      if ( $this -> problemsRepo -> deleteById( $id ) ) {

        $message = ucwords( string_to_value( $lang, $problem -> name ) );

        return response() -> json( alert_delete( $message ) );

      }

    }

  }


  public function getProblemName( Request $request, $id, $lang ) {

    $problem = $this -> problemsRepo -> findById( $id );

    $message = ucwords( string_to_value( $lang, $problem -> name) );

    return response() -> json( [ 'name' => $message ] );


  }


  public function view_problems( $data ) {

    return [

      'id' => $data -> id,

      'name' => string_to_value( lang(), $data -> name ),

      'created_at' => $data -> created_at,

      'updated_at' => $data -> updated_at

    ];

  }

  public function show_data( $lang ) {

    $problems = DB::table( 'problems' )

          -> select( [ 'id', 'name', 'created_at', 'updated_at' ] )

          -> get();

    $json = $this -> get_data( $lang  , [ 'id', 'name', 'created_at', 'updated_at' ], $problems );

    return $json;

  }

  public function get_data( $lang = '', $fields, $items = array() ) {

    $data = [];

    if ( ! empty( $items ) ) {

      foreach ( $items as $ik => $item ) {

        $key = false;

        $object = [];

        foreach ( $fields as $field ) {

          $array = $this -> to_array( $item -> $field );

          if ( is_array( $array ) ) {

            foreach ( $array as $ak => $arr ) {

              if ( $lang === $ak ) {

                $object[ $field ] = $arr;

                $key = true;

              }

              if ( $lang === '' or $lang === null ) {

                $object[ $field ] = $arr;

                $key = true;

              }

            }

          } else {

            $object[ $field ] = $item -> $field;

          }

        }

        if ( $key ) {

          $data[] = $object;

        }

      }

    }

    return (object) $data;

  }

  public function to_array( $string )  {

      $arr = json_decode( $string, true );

      return  $arr;

  }

}
