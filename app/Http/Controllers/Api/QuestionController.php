<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

use App\Repositories\General\QuestionRepository;

use App\Repositories\General\QuestionChoicesRepository;

use App\Repositories\General\QuestionnairesRepository;

use App\Repositories\General\ProblemsRepository;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Storage;

use Illuminate\Support\Facades\File;

use Illuminate\Support\Facades\DB;


class QuestionController extends Controller
{

  protected $questionRepo;

  protected $questionChoiceRepo;

  protected $questionnairesRepo;

  protected $problemRepo;

  private $disk;

  private $s3_cloude_question_image_path;


  public function __construct( QuestionRepository $questionRepo, QuestionChoicesRepository $questionChoiceRepo, QuestionnairesRepository $questionnairesRepo, ProblemsRepository $problemRepo ) {

    $this -> disk = Storage::disk( 'med4care_s3' );

    $this -> s3_cloude_question_image_path = 'questions_images';

    $this -> questionRepo = $questionRepo;

    $this -> questionChoiceRepo = $questionChoiceRepo;

    $this -> questionnairesRepo = $questionnairesRepo;

    $this -> problemRepo = $problemRepo;

  }


  public function index( Request $request ) {

    $lang = $request -> input( 'lang' );

    $data = [];

    $questions = $this -> questionRepo -> get();

    foreach ( $questions as $question ) {

      $data[] = $this -> view_questions( $question, $lang );

    }

    $question = array();

    foreach( $data as $datas ) {

      if( $datas[ 'question' ] != null ) {

        array_push( $question, [

          'id' => $datas[ 'id' ],

          'question' => $datas[ 'question' ],

          'level' => $datas[ 'level' ],

          'type' => $datas[ 'type' ],

          'status' => $datas[ 'status' ],

          'image' => $datas[ 'image' ],

          'body_parts' => $datas[ 'body_parts' ],

          'problems' => $datas[ 'problems' ],

          'isProceed' => $datas[ 'isProceed' ],

          'question_link_id' => $datas[ 'question_link_id' ],

          'created_at' => $datas[ 'created_at' ],

          'updated_at' => $datas[ 'updated_at' ],

          'choices' => $datas[ 'choices' ]

        ] );

      }

    }

    return response() -> json(

      [

        'question' => $question,

        'problem' => $this -> view_problems( $lang ),

        'body_part' => $this -> bodyPartsList( $lang )

      ]

    );

  }


  public function store( Request $request ) {

    try{

      DB::beginTransaction();

      $formData = json_decode( $request -> input( 'data' ) );

      // START DECODE BODY_PARTS TO INITIALIZED AS FINAL DATA TO BE SAVE
      $body_parts = json_decode( $request -> input( 'body_parts' ) );

      $body_parts_value = '';

      $body_parts_array = array();

      if( count( $body_parts ) > 0 ) {

        foreach( $body_parts as $parts ){

          array_push( $body_parts_array, $parts -> id);

        }

        $body_parts_value = $body_parts_array;

      }
      // END

      // START DECODE PROBLEM TO INITIALIZED AS FINAL DATA TO BE SAVE
      $problems = json_decode( $request -> input( 'problems' ) );

      $problems_value = '';

      $problems_array = array();

      if( count( $problems ) > 0 ) {

        foreach( $problems as $probs ){

          array_push( $problems_array, $probs -> id );

        }

        $problems_value = $problems_array;

      }
      // END

      // START CHECK FOR DUPLICATE ENTRY
      $question_check = $this -> questionRepo;

      if ( $question_check -> count() > 0 ) {

        foreach ( $question_check -> get() as $question_checks ) {

          $question_checks_ = $this -> view_questions( $question_checks );

          if( ucwords( $question_checks_[ 'question' ] ) == ucwords( strip_tags( $formData -> question ) ) ) {

            $message = ucwords( strip_tags( $formData -> question ) );

            return response() -> json( alert_duplicate( $message, $formData ) );

          }

        }

      }
      // END CHECK FOR DUPLICATE ENTRY
      $image_name = "no-photo.png";

      if( ! empty ( $request -> file( 'file' ) ) ) {
          
        // STORE IMAGE TO S3 BUCKET
        $filename = strtotime( date( 'Y-m-d H:i:s' ) );

        $image = $request -> file( 'file' );

        $image_name = $filename . '_' . strtolower( str_replace( ' ', '_', $image -> getClientOriginalName() ) );

        $file_contents = File::get( $image );

        $this -> disk -> put( $this -> s3_cloude_question_image_path . DIRECTORY_SEPARATOR . $image_name, $file_contents , 'public');
        // END

      }

      // INITIALIZED DATA TO BE STORED
      $data = [

        'question' => strip_tags( $formData -> question ),

        'level' => $formData -> level,

        'type' => $formData -> type,

        'status' => 0,

        'image' => $image_name,

        'body_parts' => json_encode( $body_parts_value ),

        'problems' => json_encode( $problems_value ),

        'isProceed' => $formData -> isProceed

      ];

      $jsonData = string_to_json( $formData -> language, [ 'question' ], $data );

      $question = $this -> questionRepo -> create( $jsonData );

      if( $formData -> type == 'choices' ) {

        foreach ( $formData -> choices as $choice) {

          $data_choice = [

            'choice' => $choice -> choices,

            'question_id' => $question -> id

          ];

          $jsonData_choice = string_to_json( $formData -> language, [ 'choice' ], $data_choice );

          $this -> questionChoiceRepo -> create( $jsonData_choice );

        }

      }

      // SAVE TO QUESTIONNAIRES TABLE

      DB::commit();

      $message =  ucwords( string_to_value( $formData -> language, $question[ 'question' ] ) );

      return response() -> json( alert_success( $message, $question ) );

    }

    catch( \Exception $e ) {

      echo "<pre>";

      print_r( $e -> getMessage() );

      die;

    }


  }


  public function show( $id ) {

    $question = $this -> view_questions( $this -> questionRepo -> findById( $id ) );

    $question_choice = $this -> view_questions_choices( $id );

    return response() -> json( [

      'question' => $question,

      'question_choice' => $question_choice

    ] );

  }


  public function edit( $id ) {

    $question = $this -> view_questions( $this -> questionRepo -> findById( $id ) );

    $question_choice = $this -> view_questions_choices( $id );

    return response() -> json( [

      'question' => $question,

      'question_choice' => $question_choice

    ] );

  }


  public function update( Request $request ) {

    try{

      DB::beginTransaction();

      $formData = json_decode( $request -> input( 'data' ) );

      $id = $formData -> id;

      $lang = $formData -> language;

      $body_parts = json_decode( $request -> input( 'body_parts' ) );

        $body_parts_value = '';

        $body_parts_array = array();

        if( count( $body_parts ) > 0 ) {

          foreach( $body_parts as $parts ){

            array_push( $body_parts_array, $parts -> id);

          }

          $body_parts_value = $body_parts_array;

        }

        $problems = json_decode( $request -> input( 'problems' ) );

        $problems_value = '';

        $problems_array = array();

        if( count( $problems ) > 0 ) {

          foreach( $problems as $probs ){

            array_push( $problems_array, $probs -> id);

          }

          $problems_value = $problems_array;

        }

        // CHECK FOR DUPLICATE ENTRY
        $question_check = $this -> questionRepo;

        if ( $question_check -> count() > 0 ) {

          foreach ( $question_check -> get() as $question_checks ) {

            $question_checks_ = $this -> view_questions( $question_checks );

            if( ucwords( $question_checks_[ 'question' ] ) == ucwords( strip_tags( $formData -> question ) ) && $question_checks_[ 'id' ] != $formData -> id ) {

              $message = ucwords( strip_tags( $formData -> question ) );

              return response() -> json( alert_duplicate( $message, $formData ) );

            }

          }

        }

      $filename = strtotime( date( 'Y-m-d H:i:s' ) );

      $image = $request -> file('file');

      $question = $this -> questionRepo -> findById( $id );

      $image_name = $question -> image;

      if( ! empty( $image ) ) {

        $image_name = $filename . '_' . strtolower( str_replace( ' ', '_', $image -> getClientOriginalName() ) );

        $file_contents = File::get( $image );

        $file_exists = $this -> disk -> exists( $this -> s3_cloude_question_image_path . DIRECTORY_SEPARATOR . $question -> image );

        if( $file_exists ) {

          $this -> disk -> delete( $this -> s3_cloude_question_image_path . DIRECTORY_SEPARATOR . $question -> image );

        }

        $this -> disk -> put( $this -> s3_cloude_question_image_path . DIRECTORY_SEPARATOR . $image_name, $file_contents , 'public' );

        $this -> questionRepo -> update( $question, [

          'image' => $image_name,

        ] );

      }


      $quesVal = string_add_json( $lang, $formData -> question, string_remove( $lang, $question -> question ) );

      $respQuestion = $this -> questionRepo -> update( $question, [

        'question' => strip_tags( $quesVal ),

        'level' => $formData -> level,

        'type' => $formData -> type,

        'status' => 0,

        'image' => $image_name,

        'body_parts' => json_encode( $body_parts_value ),

        'problems' => json_encode( $problems_value ),

        'isProceed' => $formData -> isProceed

      ] );

      if( $formData -> type == 'choices' ) {

        $choices_array_push = array();

        foreach( $formData -> choices as $list ) {

            if( isset( $list -> id ) && ! empty ( $list -> id ) ) {

              $choices = $this -> questionChoiceRepo -> findById( $list -> id );

              $choicesVal = string_add_json( $lang, $list -> choices, string_remove( $lang, $choices -> choice ) );

              $respQuestion = $this -> questionChoiceRepo -> update( $choices, [

                'choice' => $choicesVal,

                'question_id' => $id

              ] );

             $choices_id = $respQuestion -> id;

            } else {

              $new = array(

                'choice' => $list -> choices,

                'question_id' => $id

              );

              $jsonData = string_to_json( $lang, [ 'choice' ], $new );

              $choices = $this -> questionChoiceRepo -> create( $jsonData );

              $choices_id = $choices -> id;
            }

            array_push( $choices_array_push, $choices_id );

        }

        DB::table( 'question_choices' ) -> where( 'question_id', $id ) -> whereNotIn( 'id', $choices_array_push ) -> delete();

      }


      DB::commit();

      $message = ucwords( string_to_value( $lang, strip_tags( $quesVal ) ) );

      return response() -> json( alert_update( $message, $respQuestion ) );

    }

    catch( \Exception $e ) {

      echo "<pre>";

      print_r( $e -> getMessage() );

      die;

    }

  }


  public function saveLink( Request $request ) {

    try{

      DB::beginTransaction();

      $formData = json_decode( $request -> input( 'data' ) );

      // START DECODE BODY_PARTS TO INITIALIZED AS FINAL DATA TO BE SAVE
      $body_parts = json_decode( $request -> input( 'body_parts' ) );

      $body_parts_value = '';

      $body_parts_array = array();

      if( count( $body_parts ) > 0 ) {

        foreach( $body_parts as $parts ){

          array_push( $body_parts_array, $parts -> id );

        }

        $body_parts_value = $body_parts_array;

      }
      // END

      // START DECODE PROBLEM TO INITIALIZED AS FINAL DATA TO BE SAVE
      $problems = json_decode( $request -> input( 'problems' ) );

      $problems_value = '';

      $problems_array = array();

      if( count( $problems ) > 0 ) {

        foreach( $problems as $probs ){

          array_push( $problems_array, $probs->id);

        }

        $problems_value = $problems_array;

      }
      // END

      // START CHECK FOR DUPLICATE ENTRY
      $question_check = $this -> questionRepo;

      if ( $question_check -> count() > 0 ) {

        foreach ( $question_check -> get() as $question_checks ) {

          $question_checks_ = $this -> view_questions( $question_checks );

          if( ucwords( $question_checks_[ 'question' ] ) == ucwords( strip_tags( $formData -> question ) ) ) {

            $message = ucwords( strip_tags( $formData -> question ) );

            return response() -> json( alert_duplicate( $message, $formData ) );

          }

        }

      }
      // END CHECK FOR DUPLICATE ENTRY

      $image_name = "no-photo.png";

      if( ! empty ( $request -> file( 'file' ) ) ) {
          
        // STORE IMAGE TO S3 BUCKET
        $filename = strtotime( date( 'Y-m-d H:i:s' ) );

        $image = $request -> file( 'file' );

        $image_name = $filename . '_' . strtolower( str_replace( ' ', '_', $image -> getClientOriginalName() ) );

        $file_contents = File::get( $image );

        $this -> disk -> put( $this -> s3_cloude_question_image_path . DIRECTORY_SEPARATOR . $image_name, $file_contents , 'public' );
        // END

      }
      
      // INITIALIZED DATA TO BE STORED
      $data = [

        'question' => strip_tags( $formData->question ),

        'level' => 'secondary',

        'type' => $formData -> type,

        'status' => 0,

        'image' => $image_name,

        'body_parts' => json_encode( $body_parts_value ),

        'problems' => json_encode( $problems_value ),

        'isProceed' => 0 //$formData -> isProceed

      ];

      $jsonData = string_to_json( $formData -> language, [ 'question' ], $data );

      $question = $this -> questionRepo -> create( $jsonData );

      if( $formData -> type == 'choices' ) {

        foreach ( $formData -> choices as $choice) {

          $data_choice = [

            'choice' => $choice -> choices,

            'question_id' => $question -> id

          ];

          $jsonData_choice = string_to_json( $formData -> language, [ 'choice' ], $data_choice );

          $this -> questionChoiceRepo -> create( $jsonData_choice );

        }

      }

      $check_question_link = DB::table( 'questions' ) -> where( 'id', '=', $formData -> selected_question_id ) -> select( 'question_link_id' ) -> first() -> question_link_id;

      if( ! empty ( $check_question_link ) ) {

        $question = $this -> questionRepo -> findById( $formData -> selected_question_id );

        $message =  ucwords( string_to_value( $formData -> language, $question[ 'question' ] ) );

        return response() -> json( [

            'type' => 'cant_link',

            'message' => $message

          ] );

      }

      DB::table( 'questions' ) -> where( 'id', '=', $formData -> selected_question_id ) -> update( [ 'question_link_id' => $question -> id, 'isProceed' => 1 ] );

      if( $formData -> choice_selected !== '' ) {

        $check_question_link = DB::table( 'question_choices' ) -> where( 'id', '=', $formData -> choice_selected ) -> select( 'question_link_id' ) -> first() -> question_link_id;

        if( ! empty ( $check_question_link ) ) {

          $questionChoices = $this -> questionChoiceRepo -> findById( $formData -> choice_selected );

          $message =  ucwords( string_to_value( $formData -> language, $questionChoices[ 'choice' ] ) );

          return response() -> json( [

              'type' => 'cant_link',

              'message' => $message

            ] );

        }

        $update_question_link = $this -> questionChoiceRepo -> findById( $formData -> choice_selected );

        $respQuestion = $this -> questionChoiceRepo -> update( $update_question_link, [

          'question_link_id' => $question -> id,

        ] );

      }

      $question = $this -> questionRepo -> findById( $formData -> selected_question_id );

      // SAVE TO QUESTIONNAIRES TABLE

      DB::commit();

      $message =  ucwords( string_to_value( $formData -> language, $question[ 'question' ] ) );

      return response() -> json( alert_success( $message, $question ) );

    }

    catch( \Exception $e ) {

      echo "<pre>";

      print_r( $e -> getMessage() );

      die;

    }

  }


  public function linkExistingQuestion( Request $request ) {

    $input = json_decode( $request -> input( 'data' ) );

    if( $input -> choice_id == 'null' ) {

      $question = $this -> questionRepo -> findById( $input -> link_question_id );

      $check_question_link = DB::table( 'questions' ) -> where( 'id', '=', $input -> link_question_id ) -> select( 'question_link_id' ) -> first() -> question_link_id;

      if( ! empty ( $check_question_link ) ) {

        $question = $this -> questionRepo -> findById( $input -> link_question_id );

        $message =  ucwords( string_to_value( $input -> language, $question[ 'question' ] ) );

        return response() -> json( [

            'type' => 'cant_link',

            'message' => $message

          ] );

      }

      $respQuestion = $this -> questionRepo -> update( $question, [

        'question_link_id' => $input -> selected_question_id,

      ] );

    } else {

      $check_question_link = DB::table( 'question_choices' ) -> where( 'id', '=', $input -> choice_id ) -> select( 'question_link_id' ) -> first() -> question_link_id;

      if( ! empty ( $check_question_link ) ) {

        $questionChoices = $this -> questionChoiceRepo -> findById( $input -> choice_id );

        $message =  ucwords( string_to_value( $input -> language, $questionChoices[ 'choice' ] ) );

        return response() -> json( [

            'type' => 'cant_link',

            'message' => $message

          ] );

      }

      $update_question_link = $this -> questionChoiceRepo -> findById( $input -> choice_id );

      $this -> questionChoiceRepo -> update( $update_question_link, [

        'question_link_id' => $input -> selected_question_id,

      ] );

      $question = $this -> questionRepo -> findById( $input -> link_question_id );

      $this -> questionRepo -> update( $question, [

        'question_link_id' => $input -> selected_question_id,

      ] );

    }

    return response() -> json( alert_success( 'successfully link', $question ) );

  }


  public function destroy( Request $request, $id ) {

    $data = $request -> all();

    $lang = $request -> input( 'lang' );

    $question = $this -> questionRepo -> findById( $id );

    $file_exists = $this -> disk -> exists( $this -> s3_cloude_question_image_path . DIRECTORY_SEPARATOR . $question -> image );

    $check_question_link = DB::table( 'questions' ) -> where( 'question_link_id', '=', $id ) -> count();

    $check_choice_link = DB::table( 'question_choices' ) -> where( 'question_link_id', '=', $id ) -> count();


    $questionnaires_array = [];

    $questionnaires = $this -> questionnairesRepo -> orderBy('id', 'ASC') -> get();

    foreach ( $questionnaires as $questionnaire ) {

      $questionnaires_array[] = $this -> view_questionnaires( $questionnaire, $lang );

    }

    $questionnaires_array_value = array();

    foreach( $questionnaires_array as $datas ) {

    if( $datas[ 'questions' ] != null ) {

        array_push( $questionnaires_array_value, $datas[ 'questions' ] );

      }

    }

    $id_count = 0;

    foreach( $questionnaires_array_value as $value ) {

      foreach( $value as $key ) {

        if( $key == $id ) {

          $id_count++;

        }

      }

    }

    if( $check_question_link == 0 && $check_choice_link == 0 && $id_count == 0 ) {

      if( $file_exists ) {

        $this -> disk -> delete( $this -> s3_cloude_question_image_path . DIRECTORY_SEPARATOR . $question -> image );

      }

      if ( $this -> questionRepo -> deleteById( $id ) ) {

        $message = ucwords( string_to_value( $lang, $question -> question));

        return response() -> json( alert_delete( $message ) );

      }

    }

  }


  public function saveImages( Request $request ) {

    if( $request -> ajax() ) {

      $image = $request -> file( 'file' );

      $image_name = strtolower( $image -> getClientOriginalName() );

      $image_extension = strtolower( $image -> getClientOriginalExtension() );

      $file_contents = File::get( $image );

      $this -> disk -> put( $this -> s3_cloude_question_image_path . DIRECTORY_SEPARATOR . $image_name, $file_contents );

      return response() -> json( [ 'filename' => $image_name, 'extension' => $image_extension ] );

    }

  }


  public function view_questions( $data, $lang = '' ) {

    return [

      'id' => $data -> id,

      'question' => ucfirst( string_to_value( $lang, strip_tags( $data -> question ) ) ),

      'level' => $data -> level,

      'type' => $data -> type,

      'status' => $data -> status,

      'image' =>  $this -> disk -> url( $this -> s3_cloude_question_image_path . DIRECTORY_SEPARATOR . $data -> image ),

      'body_parts' => json_decode( $data -> body_parts ),

      'problems' => json_decode( $data -> problems ),

      'isProceed' => $data -> isProceed,

      'question_link_id' => $data -> question_link_id,

      'created_at' => $data -> created_at,

      'updated_at' => $data -> updated_at,

      'choices' => object_to_array( $lang, $this -> questionChoiceRepo -> getTableColumn(), $data -> question_choices )

    ];

  }


  public function view_questions_choice( $data, $lang = '' ) {

    return [

      'id' => $data -> id,

      'choice' => string_to_value( lang(), $data -> choice ),

      'question_id' => $data -> question_id,

      'created_at' => $data -> created_at,

      'updated_at' => $data -> updated_at,

      'questions' => object_to_array( lang(), $this -> questionRepo -> getTableColumn(), $data -> questions )

    ];

  }


  public function view_questions_choices( $id, $lang = '' ) {

    $data = [];

    if ( $this -> questionChoiceRepo -> count() > 0 ) {

      foreach ( $this -> questionChoiceRepo -> where( 'question_id', '=', $id ) -> get() as $choice ) {

        $data[] = [

          'id' => $choice -> id,

          'choice' => string_to_value( $lang, $choice -> choice )

        ];

      }

    }

    return $data;

  }


  public function view_problems( $lang ) {

    $problems = DB::table( 'problems' )

      -> select( [ 'id', 'name', 'created_at', 'updated_at' ] )

      -> get();

    $json = $this -> get_data( $lang  , [  'id', 'name', 'created_at', 'updated_at' ], $problems );

    return $json;

  }


  public function getQuestionEdit( Request $request, $id, $lang ) {

    $question = $this -> view_questions( $this -> questionRepo -> findById( $id ), $lang );

    $question_choice = $this -> view_questions_choices( $id, $lang );

    $body_parts = DB::table( 'body_parts' )

          -> select( [ 'id', 'name', 'created_at', 'updated_at' ] )

          -> get();

    $json_body_parts = $this -> get_data( $lang  , [  'id', 'name', 'created_at', 'updated_at' ], $body_parts );

    $problems = DB::table( 'problems' )

      -> select( [ 'id', 'name', 'created_at', 'updated_at' ] )

      -> get();

    $json_problems = $this -> get_data( $lang  , [  'id', 'name', 'created_at', 'updated_at' ], $problems );

    return response() -> json( [

      'question' => $question,

      'question_choice' => $question_choice,

      'body_parts' => $json_body_parts,

      'problems' => $json_problems,

      'selected_body_parts' => $question['body_parts'],

      'selected_problems' => $question['problems']

    ] );

  }


  public function getQuestionLink( Request $request, $id, $lang ) {

    $question = $this -> view_questions( $this -> questionRepo -> findById( $id ), $lang );

    $question_choice = $this -> view_questions_choices( $id, $lang );

    return response() -> json( [

      'question' => $question,

      'question_choice' => $question_choice

    ] );

  }


  public function getCollapseQuestion( Request $request, $id, $lang ) {

    $get_choice_type = DB::table( 'questions' ) -> where( 'id', '=', $id ) -> first();

    $type = 'unchoice';

    $data = [];

    if( $get_choice_type -> type == 'choices' ) {

      $type = 'choices';

      $get_choices = DB::table( 'question_choices' ) -> where( 'question_id', '=', $id ) -> select( [ 'id', 'choice', 'question_link_id' ] ) -> get();

      foreach( $get_choices as $choices_value ) {

        $link_question = '';

        $link_question_id = '';

        if( $choices_value -> question_link_id !== '' ) {

          $question_link = DB::table( 'questions' ) -> where( 'id', '=', $choices_value -> question_link_id ) -> select( 'question' ) -> first();

          if( ! empty ( $question_link ) ) {

            $link_question = string_to_value( $lang, $question_link -> question );

            $link_question_id = $choices_value -> question_link_id;

          }

        }

        $data[] = [

          'id' => $choices_value -> id,

          'choice' => string_to_value( $lang, $choices_value -> choice ),

          'question_link_id' => $link_question_id,

          'link_question' => $link_question,

          'question_id' => $id

        ];

      }

    } else {

      $question = DB::table( 'questions' ) -> where( 'id', '=', $id ) -> select('question_link_id') -> first();

      if( ! empty( $question -> question_link_id ) ) {

        $get_question = DB::table( 'questions' ) -> where( 'id', '=', $question -> question_link_id ) -> select( [ 'question', 'id' ] ) -> get();

        foreach( $get_question as $get_question_value ) {

          $data[] = [

            'id' => '',

            'choice' => '',

            'question_link_id' => $question -> question_link_id,

            'link_question' => string_to_value( $lang, $get_question_value -> question ),

            'question_id' => $id

          ];

        }

      }

    }

    return response() -> json( [ 'question' => $data, 'type' => $type ] );

  }


  public function deleteLinkQuestion( Request $request, $choice_id, $question_id ) {

    $lang = $request -> input( 'lang' );

    if( $choice_id !== 'null' ) {

      DB::table( 'question_choices' ) -> where( 'id', '=', $choice_id ) -> update( [ 'question_link_id' => NULL ] );

      if( DB::table( 'question_choices' ) -> where( 'question_id', '=', $question_id ) -> where('question_link_id', '!=', NULL ) -> count() == 0 ) {

        DB::table( 'questions' ) -> where( 'id', '=', $question_id ) -> update( [ 'question_link_id' => NULL ] );

      }

    }

    else {

      DB::table( 'questions' ) -> where( 'id', '=', $question_id ) -> update( [ 'question_link_id' => NULL ] );

    }


    $question = $this -> questionRepo -> findById( $question_id );

    $message = ucwords( string_to_value( $lang, $question -> question));

    return response() -> json( alert_delete( $message ) );

  }

  /*********************
   *
   * PRIVATE FUNCTIONS
   *
   ********************/
  private function bodyPartsList( $lang ) {

    $body_parts = DB::table( 'body_parts' )

          -> select( [ 'id', 'name', 'created_at', 'updated_at' ] )

          -> get();

    $json = $this -> get_data( $lang  , [ 'id', 'name', 'created_at', 'updated_at' ], $body_parts );

    return $json;

  }


  public function get_data( $lang = '', $fields, $items = array() ) {

    $data = [];

    if ( ! empty( $items ) ) {

      foreach ( $items as $ik => $item ) {

        $key = false;

        $object = [];

        foreach ( $fields as $field ) {

          $array = $this -> to_array( $item -> $field );

          if ( is_array( $array ) ) {

            foreach ( $array as $ak => $arr ) {

              if ( $lang === $ak ) {

                $object[ $field ] = $arr;

                $key = true;

              }

              if ( $lang === '' or $lang === null ) {

                $object[ $field ] = $arr;

                $key = true;

              }

            }

          } else {

            $object[ $field ] = $item -> $field;

          }

        }

        if ( $key ) {

          $data[] = $object;

        }

      }

    }

    return (object) $data;

  }


  public function to_array( $string )  {

      $arr = json_decode( $string, true );

      return  $arr;

  }


  public function view_questionnaires( $data, $lang = '' ) {

    return [

      'id' => $data -> id,

      'questions' => json_decode( string_to_value( $lang, $data -> questions_id ) )

    ];

  }

}
