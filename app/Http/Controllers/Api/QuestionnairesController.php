<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

use App\Repositories\General\QuestionRepository;

use App\Repositories\General\QuestionnairesRepository;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Storage;

use Illuminate\Support\Facades\File;

use Illuminate\Support\Facades\DB;

class QuestionnairesController extends Controller
{

  protected $questionRepo;

  protected $questionnairesRepo;

  private $disk;

  private $s3_cloude_question_image_path;


  public function __construct( QuestionnairesRepository $questionnairesRepo, QuestionRepository $questionRepo ) {

    $this -> disk = Storage::disk( 'med4care_s3' );

    $this -> s3_cloude_question_image_path = 'questions_images';

    $this -> questionRepo = $questionRepo;

    $this -> questionnairesRepo = $questionnairesRepo;

  }


  public function index( Request $request ) {

    $lang = $request -> input( 'lang' );

    $data = [];

    $questions = $this -> questionRepo -> get();

    foreach ( $questions as $question ) {

      $data[] = $this -> view_questions( $question, $lang );

    }


    $question = array();

    foreach( $data as $datas ) {

      if( $datas[ 'question' ] != null ) {

        array_push( $question, [

          'id' => $datas[ 'id' ],

          'name' => $datas[ 'question' ]

        ] );

      }

    }

    $questionnaires_array = [];

    $questionnaires = $this -> questionnairesRepo -> orderBy('id', 'ASC') -> get();

    foreach ( $questionnaires as $questionnaire ) {

      $questionnaires_array[] = $this -> view_questionnaires( $questionnaire, $lang );

    }

    $questionnaires_array_value = array();

    foreach( $questionnaires_array as $datas ) {

    if( $datas[ 'title' ] != null ) {

        array_push( $questionnaires_array_value, [

          'id' => $datas[ 'id' ],

          'questions' => $datas[ 'questions' ],

          'title' => $datas[ 'title' ],

          'image' => $datas[ 'image' ],

          'user_id' => $datas[ 'user_id' ],

          'created_at' => $datas[ 'created_at' ],

          'updated_at' => $datas[ 'updated_at' ]

        ] );

      }

    }

    return response() -> json(

      [

        'questionnaire' => $questionnaires_array_value,

        'question' => $question

      ]

    );

  }


  public function store( Request $request ) {

    try{

      DB::beginTransaction();

      $formData = json_decode( $request -> input( 'data' ) );

      // START CHECK FOR DUPLICATE ENTRY
      $question_check = $this -> questionnairesRepo;

      if ( $question_check -> count() > 0 ) {

        foreach ( $question_check -> get() as $question_checks ) {

          if( ucwords( $question_checks -> title ) == ucwords( strip_tags( $formData -> name ) ) ) {

            $message = ucwords( strip_tags( $formData -> name ) );

            return response() -> json( alert_duplicate( $message, $formData ) );

          }

        }

      }
      // END CHECK FOR DUPLICATE ENTRY

      // START DECODE QUESTION TO INITIALIZED AS FINAL DATA TO BE SAVE

      $questionnaires_value = '';

      $questionnaires_array = array();

      if( count( $formData -> question ) > 0 ) {

        foreach( $formData -> question as $questionnaires ){

          array_push( $questionnaires_array, $questionnaires -> id );

        }

        $questionnaires_value = $questionnaires_array;

      }

      $image_name = "no-photo.png";

      if( ! empty ( $request -> file( 'file' ) ) ) {
          
        // STORE IMAGE TO S3 BUCKET
        $filename = strtotime( date( 'Y-m-d H:i:s' ) );

        $image = $request -> file( 'file' );

        $image_name = $filename . '_' . strtolower( str_replace( ' ', '_', $image -> getClientOriginalName() ) );

        $file_contents = File::get( $image );

        $this -> disk -> put( $this -> s3_cloude_question_image_path . DIRECTORY_SEPARATOR . $image_name, $file_contents , 'public');
        // END

      }
      
      $data = [

        'questions_id' => json_encode( $questionnaires_value ),

        'title' => $formData -> name,

        'image' => $image_name,

        'user_id' => 1

      ];

      $jsonData = string_to_json( $formData -> language, [ 'title', 'questions_id' ], $data );

      $question = $this -> questionnairesRepo -> create( $jsonData );

      DB::commit();

      $message =  ucwords( $formData -> name );

      return response() -> json( [ 'success' => true, 'message' => $message ] );

    }

    catch( \Exception $e ) {

      echo "<pre>";

      print_r( $e -> getMessage() );

      die;

    }

  }


  public function update( Request $request ) {

    try{

      DB::beginTransaction();

      $formData = json_decode( $request -> input( 'data' ) );

      $id = $formData -> id;

      $lang = $formData -> language;

        // CHECK FOR DUPLICATE ENTRY
        $questionnaire_check = $this -> questionnairesRepo;

        if ( $questionnaire_check -> count() > 0 ) {

          foreach ( $questionnaire_check -> get() as $questionnaire_checks ) {

            $questionnaire_checks_ = $this -> view_questionnaires( $questionnaire_checks, $lang );

            if( ucwords( $questionnaire_checks_[ 'title' ] ) == ucwords( strip_tags( $formData -> name ) ) && $questionnaire_checks_[ 'id' ] != $formData -> id ) {

              $message = ucwords( strip_tags( $formData -> title ) );

              return response() -> json( alert_duplicate( $message, $formData ) );

            }

          }

        }


      $filename = strtotime( date( 'Y-m-d H:i:s' ) );

      $image = $request -> file('file');

      $question = $this -> questionnairesRepo -> findById( $id );

      $image_name = $question -> image;

      if( ! empty( $image ) ) {

        $image_name = $filename . '_' . strtolower( str_replace( ' ', '_', $image -> getClientOriginalName() ) );

        $file_contents = File::get( $image );

        $file_exists = $this -> disk -> exists( $this -> s3_cloude_question_image_path . DIRECTORY_SEPARATOR . $question -> image );

        if( $file_exists ) {

          $this -> disk -> delete( $this -> s3_cloude_question_image_path . DIRECTORY_SEPARATOR . $question -> image );

        }

        $this -> disk -> put( $this -> s3_cloude_question_image_path . DIRECTORY_SEPARATOR . $image_name, $file_contents , 'public' );

        $this -> questionnairesRepo -> update( $question, [

          'image' => $image_name,

        ] );

      }

      $quesVal = string_add_json( $lang, $formData -> name, string_remove( $lang, $question -> title ) );

      $update_questionnaires = $formData -> question;

      $update_questionnaires_value = '';

      $update_questionnaires_array = array();

      if( count( $update_questionnaires ) > 0 ) {

        foreach( $update_questionnaires as $update_questionnaire ){

          array_push( $update_questionnaires_array, $update_questionnaire -> id );

        }

        $update_questionnaires_value = $update_questionnaires_array;

      }

      $question_id_val = string_add_json( $lang, json_encode( $update_questionnaires_value ), string_remove( $lang, $question -> questions_id ) );

      $respQuestion = $this -> questionnairesRepo -> update( $question, [

          'questions_id' => $question_id_val,

          'title' => $quesVal,

          'image' => $image_name,

          'user_id' => 1

      ] );

      DB::commit();

      $message = ucwords( string_to_value( $lang, strip_tags( $quesVal ) ) );

      return response() -> json( alert_update( $message, $respQuestion ) );

    }

    catch( \Exception $e ) {

      echo "<pre>";

      print_r( $e -> getMessage() );

      die;

    }

  }


  public function destroy( Request $request, $id ) {

    $data = $request -> all();

    $lang = $request -> input( 'lang' );

    $questionnaire = $this -> questionnairesRepo -> findById( $id );

    $file_exists = $this -> disk -> exists( $this -> s3_cloude_question_image_path . DIRECTORY_SEPARATOR . $questionnaire -> image );

    if( $file_exists ) {

      $this -> disk -> delete( $this -> s3_cloude_question_image_path . DIRECTORY_SEPARATOR . $questionnaire -> image );

    }

    if ( $this -> questionnairesRepo -> deleteById( $id ) ) {

      $message = ucwords( string_to_value( $lang, $questionnaire -> title));

      return response() -> json( alert_delete( $message ) );

    }

  }


  public function view_questions( $data, $lang = '' ) {

    return [

      'id' => $data -> id,

      'question' => ucfirst( string_to_value( $lang, strip_tags( $data -> question ) ) )

    ];

  }


  public function view_questionnaires( $data, $lang = '' ) {

    return [

      'id' => $data -> id,

      'questions' => json_decode( string_to_value( $lang, $data -> questions_id ) ),

      'title' => ucfirst( string_to_value( $lang, strip_tags( $data -> title ) ) ),

      'image' => $this -> disk -> url( $this -> s3_cloude_question_image_path . DIRECTORY_SEPARATOR . $data -> image ),

      'user_id' => $data -> user_id,

      'created_at' => $data -> created_at,

      'updated_at' => $data -> updated_at

    ];

  }


  public function getQuestionnaireName( Request $request, $id, $lang ) {

    $title = "";

    $questionnaire_extract = array();

    $questionnaire = $this -> questionnairesRepo -> findById( $id );

    $questions_id = ucwords( string_to_value( $lang, $questionnaire -> questions_id ) );

    if( ! empty ( $questions_id ) ) {

      $title = ucwords( string_to_value( $lang, $questionnaire -> title ) );

      $decode_question_id = json_decode($questions_id);

      $storeValue = [];
      foreach ($decode_question_id as $value) {

        $storeValue[] = $this -> questionRepo -> findById( $value );

      }

      $extract = [];
      foreach ( $storeValue as $storeValues ) {

        $extract[] = $this -> view_questions( $storeValues, $lang );

      }

      foreach( $extract as $extracts ) {

        if( $extracts[ 'question' ] != null ) {

          array_push( $questionnaire_extract, [

            'id' => $extracts[ 'id' ],

            'name' => $extracts[ 'question' ]

          ] );

        }

      }

    }

    $data = [];

    $questions = $this -> questionRepo -> get();

    foreach ( $questions as $question ) {

      $data[] = $this -> view_questions( $question, $lang );

    }

    $question = array();

    foreach( $data as $datas ) {

      if( $datas[ 'question' ] != null ) {

        array_push( $question, [

          'id' => $datas[ 'id' ],

          'name' => $datas[ 'question' ]

        ] );

      }

    }

    return response() -> json( [ 'success' => true, 'title' => $title, 'questions_id' => $questionnaire_extract, 'question' => $question ] );

  }


  public function fetchQuestionnaire( $id, $lang ) {

    $questionnaire = $this -> questionnairesRepo -> findById( $id );

    $questions = json_decode( ucwords( string_to_value( $lang, $questionnaire -> questions_id ) ) );

    $data = [];

    foreach( $questions as $question ) {

      $data[] = DB::table( 'questions' ) -> where( 'id', '=', $question ) -> select( [ 'id', 'question' ] ) -> first();

    }

    $question = array();

    foreach( $data as $datas ) {

      if( $datas -> question != null ) {

        array_push( $question, [

          'id' => $datas -> id,

          'name' => ucwords( string_to_value( $lang, $datas -> question ) )

        ] );

      }

    }

    return response() -> json( [ 'questionnaires_list' => $question ] );

  }


  public function deleteQuestionInQuestionnaire( Request $request, $questionnaire_id, $question_id ) {

    $lang = $request -> input( 'lang' );

    $questionnaire = $this -> questionnairesRepo -> findById( $questionnaire_id );

    $questions = json_decode( ucwords( string_to_value( $lang, $questionnaire -> questions_id ) ) );

    $question_new = array();

    foreach( $questions as $question ) {

      if( $question != $question_id ) {

        array_push( $question_new, $question );

      }

    }

    $question_id_val = string_add_json( $lang, json_encode( $question_new ), string_remove( $lang, $questionnaire -> questions_id ) );

      $respQuestion = $this -> questionnairesRepo -> update( $questionnaire, [

          'questions_id' => $question_id_val

      ] );

      $question_title = $this -> questionRepo -> findById( $question_id );

      $message = ucwords( string_to_value( $lang, $question_title -> question ) );

      return response() -> json( [

          'success' => true,

          'message' => $message,

          'questionnaires_list' => $this -> fetchQuestionnaire( $questionnaire_id, $lang )

       ] );

  }


}
