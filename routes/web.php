<?php
use Dingo\Api\Routing\Router;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/


/** @var Router $api */
$api = app(Router::class);

$api->version('v1', function (Router $api) {

	// Routes that needs admin auth
	$api->group( [ 'prefix' => 'admin', 'middleware' => [ 'auth', 'user_administrator' ] ], function(Router $api) {

		$api->group( [ 'prefix' => 'questions' ], function( Router $api ) {

			$api->get('/', 'App\\Http\\Controllers\\Api\\QuestionController@index');

			$api->post('create', 'App\\Http\\Controllers\\Api\\QuestionController@store');

			$api->get('{id}', 'App\\Http\\Controllers\\Api\\QuestionController@show');

			$api->get('{id}/edit', 'App\\Http\\Controllers\\Api\\QuestionController@edit');

			$api->post('update', 'App\\Http\\Controllers\\Api\\QuestionController@update');

			$api->get('delete/{id}', 'App\\Http\\Controllers\\Api\\QuestionController@destroy');

			$api->get('getQuestionEdit/{id}/{lang}', 'App\\Http\\Controllers\\Api\\QuestionController@getQuestionEdit');

			$api->get('getQuestionLink/{id}/{lang}', 'App\\Http\\Controllers\\Api\\QuestionController@getQuestionLink');

			$api->get('getCollapseQuestion/{id}/{lang}', 'App\\Http\\Controllers\\Api\\QuestionController@getCollapseQuestion');

			$api->get('deleteLinkQuestion/{choice_id}/{question_id}', 'App\\Http\\Controllers\\Api\\QuestionController@deleteLinkQuestion');

			$api->post('linkExistingQuestion', 'App\\Http\\Controllers\\Api\\QuestionController@linkExistingQuestion');

			// IMAGE
			$api->post('save/image', 'App\\Http\\Controllers\\Api\\QuestionController@saveImages');

			// LINK
			$api->post('link', 'App\\Http\\Controllers\\Api\\QuestionController@saveLink');

		} );

		$api->group( [ 'prefix' => 'surgical' ], function( Router $api ) {

			$api->get('/', 'App\\Http\\Controllers\\Api\\SurgicalProcedureController@index');

			$api->post('create', 'App\\Http\\Controllers\\Api\\SurgicalProcedureController@store');

			$api->get('{id}', 'App\\Http\\Controllers\\Api\\SurgicalProcedureController@show');

			$api->get('{id}/edit', 'App\\Http\\Controllers\\Api\\SurgicalProcedureController@edit');

			$api->post('update', 'App\\Http\\Controllers\\Api\\SurgicalProcedureController@update');

			$api->get('delete/{id}', 'App\\Http\\Controllers\\Api\\SurgicalProcedureController@destroy');

			$api->get('getProceduralName/{id}/{lang}', 'App\\Http\\Controllers\\Api\\SurgicalProcedureController@getProceduralName');

		} );

		$api->group( [ 'prefix' => 'problems' ], function( Router $api ) {

			$api->get('/', 'App\\Http\\Controllers\\Api\\ProblemsController@index');

			$api->post('create', 'App\\Http\\Controllers\\Api\\ProblemsController@store');

			$api->get('{id}', 'App\\Http\\Controllers\\Api\\ProblemsController@show');

			$api->get('{id}/edit', 'App\\Http\\Controllers\\Api\\ProblemsController@edit');

			$api->post('update', 'App\\Http\\Controllers\\Api\\ProblemsController@update');

			$api->get('delete/{id}', 'App\\Http\\Controllers\\Api\\ProblemsController@destroy');

			$api->get('getProblemName/{id}/{lang}', 'App\\Http\\Controllers\\Api\\ProblemsController@getProblemName');

		} );

		$api->group( [ 'prefix' => 'questionnaires' ], function( Router $api ) {

			$api->get('/', 'App\\Http\\Controllers\\Api\\QuestionnairesController@index');

			$api->post('create', 'App\\Http\\Controllers\\Api\\QuestionnairesController@store');

			$api->post('update', 'App\\Http\\Controllers\\Api\\QuestionnairesController@update');

			$api->get('delete/{id}', 'App\\Http\\Controllers\\Api\\QuestionnairesController@destroy');

			$api->get('getQuestionnaireName/{id}/{lang}', 'App\\Http\\Controllers\\Api\\QuestionnairesController@getQuestionnaireName');

			$api->get('fetchQuestionnaire/{id}/{lang}', 'App\\Http\\Controllers\\Api\\QuestionnairesController@fetchQuestionnaire');

			$api->get('deleteQuestionInQuestionnaire/{questionnaire_id}/{question_id}', 'App\\Http\\Controllers\\Api\\QuestionnairesController@deleteQuestionInQuestionnaire');

		} );

	});


	// ROUTE FOR DOCTORS ROLE
	$api->group( [ 'prefix' => 'doctor', 'middleware' => [ 'auth', 'doctor' ] ], function(Router $api) {

	} );

	// OPEN ROUTE FOR PULLING QUESTION AND QUESTIONNAIRES
	$api->group( [ 'prefix' => 'data' ], function(Router $api) {

		$api->get('/questions', 'App\\Http\\Controllers\\Api\\QuestionController@index');

		$api->get('/questionnaires', 'App\\Http\\Controllers\\Api\\QuestionnairesController@index');

	} );

});
