<?php

/*
 |--------------------------------------------------------------------------
 | Application Name
 |--------------------------------------------------------------------------
 |
 | This value is the name of your application. This value is used when the
 | framework needs to place the application's name in a notification or
 | any other location as required by the application or its packages.
 |
 */

return [
  'admin'  => env('ROLE_ADMINISTRATION', null),
  'user'   => env('ROLE_USER', null),
  'client' => env('ROLE_CLIENT', null)
];
