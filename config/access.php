<?php

return [
  /*
  |--------------------------------------------------------------------------
  | Application Name
  |--------------------------------------------------------------------------
  |
  | This value is the name of your application. This value is used when the
  | framework needs to place the application's name in a notification or
  | any other location as required by the application or its packages.
  |
  */
  "tablenames" => [
    'questions'          => 'questions',

  ],

  /*
   |--------------------------------------------------------------------------
   | Application Name
   |--------------------------------------------------------------------------
   |
   | This value is the name of your application. This value is used when the
   | framework needs to place the application's name in a notification or
   | any other location as required by the application or its packages.
   |
   */

  's3_path' => [

    'brands'      => env('BRAND_PATH', ''),

    'instructors' => env('INSTRUCTOR_PATH', ''),

    'questions_images' => env('MED4CARE_PATH','')
  ],

  'api' => [
    'check_auth' => env('API_END_POINT_AUTH_USER', ''),
    'check_user_role' => env('API_END_POINT_USER_ROLE', '')
  ],

];
